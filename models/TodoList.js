const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TodoListSchema = new Schema({
    title_id : {
        type: String,
        require: true
    },
    description:{
        type: String,
        required: true
    },
    deadline: {
        type: String,
        required: false,
    },
    status: {
        type: Boolean,
        required: false,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model('TodoList',  TodoListSchema);