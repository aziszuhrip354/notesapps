const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TodoListTitleSchema = new Schema({
    user_id : {
        type: String,
        required: true
    },
    title:{
        type: String,
        required: true
    },
    type_todolist: {
        type: String,
        require: true
    }
}, {timestamps: true})

module.exports = mongoose.model('TodoListTitle',  TodoListTitleSchema);