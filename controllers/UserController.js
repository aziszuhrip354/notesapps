const  user = require("../models/User");
const bcrypt = require("bcrypt");
const req = require("express/lib/request");
const User = require("../models/User");
const { emit } = require("../models/TodoList");
const { get, json } = require("express/lib/response");
const TodoList = require("../models/TodoList");
const TodoListTitle = require("../models/TodoListTitle");

const createUser = async (req, res) => {
    const {firstname, lastname, email} = req.body;
    let password = req.body.password;
    try {
        if(!firstname) {
            throw 'firstname required';
        } 

        if(!lastname) {
            throw 'lastname required';
        } 

        if(!email) {
            throw 'email required';
        } 

        if(!password) {
            throw 'password required';
        }

        const checkUserExistByEmail = await User.findOne({email});
        if(checkUserExistByEmail) {
           throw "email was created!";
        }

        const salt = await bcrypt.genSalt(10);
        password = await bcrypt.hash(password, salt);
        const newUser = await user.create({
            firstname,
            lastname,
            email,
            password
        });
        res.json({
            status: true,
            data: newUser,
            message: "success created new user"
        });
    } catch (error) {
        res.status(400).json({status: false, message: "failed to created new user!", errors: error.message ?? error});
    }
}

const getDataUser = async (req, res) => {
    let responses = {
        status: true,
        code: 200,
        data: {
            
        }
    }

    const {user_id} = req.params;

    try {
        const getTotalNotes = await TodoListTitle.find({user_id, type_todolist: "unlimited"}).count();
        const getTotalTodolist = await TodoListTitle.find({user_id, type_todolist: "limited"}).count();
        responses.message = "success to get data user";
        responses.data.total_todolist = getTotalTodolist;
        responses.data.total_notes = getTotalNotes;
        res.status(responses.code).json(responses);
    } catch (error) {
        responses.status = false;
        responses.code = 400;
        responses.message = "failed to get data user";
        responses.error = error.message ?? error;
        res.status(responses.code).json(responses);
    }
}

const login = async (req, res) => {
    let responses = {
        status: true,
        code: 200,
    }
    const {email, password} = req.body;
    try {
        if(!password || !email) {
            throw "email and password is required";
        }
        const getUser = await User.findOne({email});
        if(!getUser) {
            throw "email or password is not correct!";
        }
        console.log("test1...");
        const validPassword = await bcrypt.compare(password, getUser.password);
        
        console.log("test2...");

        if(!validPassword) {
            throw "email or password is not correct!";
        }

        const user_id = getUser._id;
        responses.message = "success login";
        responses.data = getUser;
        res.status(responses.code).json(responses);
    } catch (error) {
        responses.status = false;
        responses.code = 400;
        responses.message = "failed to login";
        responses.error = error.message ?? error;
        res.status(responses.code).json(responses);
    }
}

module.exports = {
    createUser,
    login,
    getDataUser,
}