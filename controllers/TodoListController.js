const todoList = require("../models/TodoList");
const todoListTitle = require("../models/TodoListTitle");
const User = require("../models/User");
const mongoose = require("mongoose");
const TodoList = require("../models/TodoList");
const { count } = require("../models/TodoList");
const { search } = require("../routes/TodoListRoutes");
const TodoListTitle = require("../models/TodoListTitle");

// TODO LIST
const createTodoList = async (request, response) => {
    let {title_id, description, deadline} = request.body;
    description = description.toUpperCase();
    try {
        if(!title_id) {
            throw "title_id is required";
        }

        if(!description) {
            throw "description is required";
        }

        const checkObjectId = mongoose.Types.ObjectId.isValid(title_id);
        if(!checkObjectId) {
            throw "title id not valid";
        }
        const titleId = mongoose.Types.ObjectId(title_id);
        const checkTodoListTitleValid = await TodoListTitle.findOne({_id: titleId});
        if(!checkTodoListTitleValid) {
            throw "title id not found!";
        }
        console.log(checkTodoListTitleValid.type_todolist);
        if(checkTodoListTitleValid.type_todolist == "limited" && !deadline) {
            throw "deadline is required";
        }

        const checkTodoList = await todoList.findOne({title_id, description});
        if(checkTodoList) {
            throw "todo list was created!";
        }
        const newTodoList = await todoList.create({title_id, description, deadline});
        response.json({
            status: true,
            data: newTodoList,
            message: "success created new todolist"
        });
    } catch (error) {
        response.status(400).json({status: false, message: "failed to created new todolist!", error: error.message ?? error});
    }
}

const getTodoListes = async (req, res) => {
    let responses = {
        status: true,
        code: 200,
    }
    let {sort, total_data, search, statusTodolist } = req.query;
    const title_id = req.params.title_id;
    let order;
    switch (sort) {
        case "desc":
            order = -1;
            break;
        default:
            order = 1;
            break;
    }
    try {
        let getTodoListes;
        const titleId = mongoose.Types.ObjectId(title_id);
        const checkTodoListTitle = await TodoListTitle.findOne({_id: titleId});
        if(!checkTodoListTitle) {
            throw "title_id not found";
        }

        if(total_data != "all") {
            getTodoListes = await TodoList.find({title_id}).limit(parseInt(total_data)).sort({createdAt: order});
        } else {
            getTodoListes = await TodoList.find({title_id}).sort({createdAt: order});
        }

        if(statusTodolist != "all") {
            statusTodolist = statusTodolist == "true" ? true : false;
            getTodoListes = await TodoList.find({title_id, status: statusTodolist}).limit(parseInt(total_data)).sort({createdAt: order});
        }

        if(search) {
           search = search.toUpperCase();
           getTodoListes = await TodoList.find({title_id, description:{$regex: `.*${search}.*`}}).limit(parseInt(total_data)).sort({createdAt: order});
        }

        if(getTodoListes.length == 0) {
            throw "todo list not found";
        }
        responses.message = "success to get all todolist";
        responses.data = getTodoListes;
    } catch (error) {
        responses.message = !error.message ? "data not found" : "failed to get data";
        responses.code = 200;
        responses.error = error.message ?? error;
    }
    res.status(responses.code).json(responses);
}

const updateTodoList = async (req, res) => {
    let responses = {
        status: true,
        code : 200,
    }

    let {todo_id, description} = req.body;
    description = description.toUpperCase();
    try {
        if(!todo_id) {
            throw "todo_id is required";
        }
        if(!description) {
            throw "description is required";
        }
        const todoId = mongoose.Types.ObjectId(todo_id);
        const updateTodoList = await todoList.updateOne({_id: todoId},{$set: {description}});
        if(updateTodoList.modifiedCount == 0 ) {
            throw "data todolist not found";
        }
        responses.message = "success update todolist";
    } catch (error) {
        responses.code = 400;
        responses.message = error.message ? "failed to delete todolist" : error;
        responses.error = error.message ?? null;
    }

    res.status(responses.code).json(responses);
}


const changeStatusTodoList = async (req, res) => {
    let responses = {
        status: true,
        code : 200,
    }

    const {todo_id} = req.body;
    try {
        if(!todo_id) {
            throw "todo_id is required";
        }
        const status = true;
        const todoId = mongoose.Types.ObjectId(todo_id);
        const updateTodoList = await todoList.updateOne({_id: todoId},{$set: {status}});
        if(updateTodoList.modifiedCount == 0 ) {
            throw "data todolist not found";
        }
        responses.message = "success to finished todolist";
    } catch (error) {
        responses.code = 400;
        responses.message = error.message ? "failed to delete todolist" : error;
        responses.error = error.message ?? null;
    }

    res.status(responses.code).json(responses);
}

const deleteTodoList = async (req, res) => {
    let responses = {
        status: true,
        code : 200,
    }

    const {todo_id} = req.body;
    try {
        if(!todo_id) {
            throw "todo_id is required";
        }
        const todoId = mongoose.Types.ObjectId(todo_id);
        const deleteTodoList = await todoList.deleteOne({_id: todoId});
        if(deleteTodoList.deletedCount == 0 ) {
            throw "data todolist not found";
        }
        responses.message = "success deleted todolist";
    } catch (error) {
        responses.code = 400;
        responses.message = error.message ? "failed to delete todolist" : error;
        responses.error = error.message ?? null;
    }

    res.status(responses.code).json(responses);
}


// TODO LIST TITLE
const createTodoListTitle = async (request, response) => {
    let {user_id, title, type_todolist} = request.body;
    try {
        if(!user_id) {
            throw "user_id is required";
        }

        if(!type_todolist) {
            throw "type_todolist is required";
        }

        if(type_todolist != "limited" && type_todolist != "unlimited") {
            throw "type_todolist is not valid"
        }


        if(!title) {
            throw "title is required";
        }
        title = title.toUpperCase();

        const checkObjectId = mongoose.Types.ObjectId.isValid(user_id);
        if(!checkObjectId) {
            throw "user id not valid";
        }
        const userId = mongoose.Types.ObjectId(user_id);
        const checkUseValid = await User.find({_id: userId});
        if(!checkUseValid) {
            throw "user not found!";
        }
        const checkTodoListTitle = await todoListTitle.findOne({user_id, title});
        if(checkTodoListTitle) {
            throw "todo list title was created!";
        }
        const newTodoListTitle = await todoListTitle.create({user_id, title, type_todolist});
        response.json({
            status: true,
            data: newTodoListTitle,
            message: "success created new todolist title"
        });
    } catch (error) {
        response.status(400).json({status: false, message: "failed to created new todolist title!", error: error.message ?? error});
    }
}

const getTodoListTitles = async (req, res) => {
    let responses = {
        status: true,
        code: 200,
    }
    const user_id = req.params.user_id;
    let {sort, total_data, search, type } = req.query;
    console.log(search);
    let order;
    switch (sort) {
        case "desc":
            order = -1;
            break;
        default:
            order = 1;
            break;
    }
    try {
        console.log("test 1");
        const userId = mongoose.Types.ObjectId(user_id);
        const checkUser = await User.find({_id: userId});
        if(!checkUser) {
            throw "user not found";
        }
        let getTodoListTitles;
        if(total_data != "all") {
            console.log(order, user_id, total_data)
            getTodoListTitles = await todoListTitle.find({user_id}).limit(parseInt(total_data)).sort({createdAt: order});
        } else {
            console.log("test 2 2");
            getTodoListTitles = await todoListTitle.find({user_id}).sort({createdAt: order});
        }

        if(type != "all") {
            getTodoListTitles = await todoListTitle.find({user_id, type_todolist: type}).limit(parseInt(total_data)).sort({createdAt: order});
        }

        if(search) {
            search = search.toUpperCase();
            getTodoListTitles = await todoListTitle.find({user_id, title:{$regex: `.*${search}.*`}}).limit(parseInt(total_data)).sort({createdAt: order});
        }

        
        if(getTodoListTitles.length == 0) {
            throw "todo list not found";
        }
        console.log("test 3");
        var result = [];
        await Promise.all(getTodoListTitles.map(async val => {
            let done = await TodoList.find({title_id: val._id, status: true}).count();
            let undone = await TodoList.find({title_id: val._id, status: false}).count();
            let newVal = {
                createdAt: val.createdAt,
                title: val.title,
                type_todolist: val.type_todolist,
                updatedAt: val.updatedAt,
                user_id:val.user_id,
                _id : val._id,
                total_todolist_done:  done,
                total_todolist_undone:  undone,
            };
            result.push(newVal);
            console.log(newVal);
        }));
        console.log("test");
        responses.message = "success to get all todolist title";
        responses.data = result;
    
    } catch (error) {
        responses.message = !error.message ? "data not found" : "failed to get data";
        responses.code = 400;
        responses.error = error.message ?? error;
    }
    res.status(responses.code).json(responses);
}

const getTodoListTitle = async (req, res) => {
    let responses = {
        status: true,
        code: 200,
    }
    const id = req.params.id;
    try {
        const titleId = mongoose.Types.ObjectId(id);
        const getTodoListTitles = await todoListTitle.findOne({_id: titleId});
        if(!getTodoListTitles) {
            throw "todo list not found";
        }
        responses.message = "success to get todolist title";
        responses.data = getTodoListTitles;
    } catch (error) {
        responses.message = !error.message ? "data not found" : "failed to get data";
        responses.code = 200;
        responses.error = error.message ?? error;
    }
    res.status(responses.code).json(responses);
}

const deleteTodoListTitle = async (req, res) => {
    let responses = {
        status: true,
        code : 200,
    }

    const {title_id} = req.body;
    try {
        if(!title_id) {
            throw "title id is required";
        }
        const titleId = mongoose.Types.ObjectId(title_id);
        const deleteTodoListTitle = await todoListTitle.deleteOne({_id: titleId});
        if(deleteTodoListTitle.deletedCount == 0 ) {
            throw "data todolist title not found";
        }
        responses.message = "success deleted todolist title";
        const deleteTodoList = await todoList.deleteMany({title_id: titleId});
        if(deleteTodoList.deletedCount > 0) {
            responses.total_deleted_todolist = deleteTodoList.deletedCount;
        }
    } catch (error) {
        responses.code = 400;
        responses.message = error.message ? "failed to delete todolist title" : error;
        responses.error = error.message ?? null;
    }

    res.status(responses.code).json(responses);
}

const updateTodoListTitle = async (req, res) => {
    let responses = {
        status: true,
        code : 200,
    }

    let {title_id, title, type_todolist} = req.body;
    try {
        if(!title_id) {
            throw "title_id is required";
        }

        if(!type_todolist) {
            throw "type_todolist is required";
        }

        if(!title) {
            throw "title is required";
        }
        title = title.toUpperCase();
        const titleId = mongoose.Types.ObjectId(title_id);
        const updateTodoListTitle = await todoListTitle.updateOne({_id: titleId},{$set: {title: title, type_todolist}});
        if(updateTodoListTitle.modifiedCount == 0 ) {
            throw "data todolist title not found";
        }
        responses.message = "success update todolist title";
    } catch (error) {
        responses.code = 400;
        responses.message = error.message ? "failed to update todolist title" : error;
        responses.error = error.message ?? null;
    }

    res.status(responses.code).json(responses);
}

module.exports = {
    // Method TodoList Title
    createTodoListTitle,
    getTodoListTitles,
    getTodoListTitle,
    deleteTodoListTitle,
    updateTodoListTitle,
    // Method TodoList
    createTodoList,
    getTodoListes,
    updateTodoList,
    changeStatusTodoList,
    deleteTodoList,
}