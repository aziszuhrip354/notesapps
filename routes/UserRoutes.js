const { Route } = require("express");
require('express-group-routes');
const app =  require("express");
const { createUser, login, getDataUser } = require("../controllers/UserController");
const router = app.Router();

router.post("/user", createUser)
      .post("/user/login", login)
      .get("/user/:user_id", getDataUser)
      .get("/user", createUser);


module.exports = router;