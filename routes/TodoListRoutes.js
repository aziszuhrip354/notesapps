const { Route } = require("express");
require('express-group-routes');
const app =  require("express");
const router = app.Router();
const { 
    createTodoList,
    createTodoListTitle,
    getTodoListTitles,
    getTodoListTitle,
    deleteTodoListTitle,
    updateTodoListTitle,
    getTodoListes,
    updateTodoList,
    deleteTodoList,
    changeStatusTodoList
} = require("../controllers/TodoListController");

router.post("/todolist", createTodoList)
      .delete("/todolist", deleteTodoList)
      .put("/todolist", updateTodoList)
      .put("/todolist/change-status", changeStatusTodoList)
      .get("/todolist/all/:title_id", getTodoListes)

router.post("/todolist-title", createTodoListTitle)
      .delete("/todolist-title", deleteTodoListTitle)
      .put("/todolist-title", updateTodoListTitle)
      .get("/todolist-title/:id", getTodoListTitle)
      .get("/todolist-title/all/:user_id", getTodoListTitles);



module.exports = router;