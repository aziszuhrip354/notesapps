require("dotenv").config();

const express = require("express");
const req = require("express/lib/request");
const TodoListRoutes = require("./routes/TodoListRoutes");
const UserRoutes = require("./routes/UserRoutes");
const mongoose = require("mongoose");
const User = require("./models/User");
const listEndpoints = require("express-list-endpoints"); 
const cors = require('cors');
const bodyParser = require('body-parser');


// EXPRESS APP
const app = express();

// MIDDLEWARE 
app.use(cors());
app.use(express.json());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use((req, res, next) => {
    console.log(req.path, req.method);
    next();
});
 
// ROUTES 
app.use("/api", TodoListRoutes);
app.use("/api/", UserRoutes);

mongoose.connect(process.env.MONGO_URI).then(() => {
    // LISTEN APP
    app.listen(process.env.PORT, () => {
        console.log(`MONGODB ACTIVED & SERVER RUN AT http://localhost:${process.env.PORT}/`);
        console.log("List Routes: ",listEndpoints(app));;
    })
}).catch(err => {
    console.log(`Mongo error : ${err}`);
});

